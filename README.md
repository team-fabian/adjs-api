// MEETUP API

// **\*\***\*\*\*\***\*\***User Routes**\*\***\*\***\*\***

// create User Request

post:url/user/store

// Get all users Request
get:url/user/all

// login
post:url/user/login

// logout

post:url/user/logout

// get specific user

get : url/user/`${id}`

// delete specific user and all its groups and member data

delete : url/user/`${id}`

// **\*\*\*\***\*\*\***\*\*\*\***Group Routes**\*\*\*\***\*\***\*\*\*\***

post:url/group/store

get: url/group/all

get: url/group/`${id}

// delete specific group and all its groups and member data

delete : url/group/`${id}

// \***\*\*\*\*\***\*\*\*\***\*\*\*\*\***Member Routes\***\*\*\*\*\*\*\***\*\***\*\*\*\*\*\*\***

post: url/member/store

// Get all members Request

get: url/member/all

// get all members of a specific group

get: url/member/getMembers/`@{id}`

//\***\*\*\*\*\*\*\***\*\*\*\***\*\*\*\*\*\*\***Events

Routes\***\*\*\*\*\*\*\***\*\*\*\***\*\*\*\*\*\*\***

post:url/event/store

// Get all groups Request

get: url/event/all

// Get Specific group
get: url/event/`${id}`

// delete event
delete: url/event/`${id}`

//\***\*\*\*\*\*\*\***\*\*\*\*\***\*\*\*\*** attendees Routes \***\*\*\*\*\***\*\*\***\*\*\*\*\***

// add attendee
post: url/attendee/store

// Get all attendee Request

get: url/attendee/all

// get all members of a specific group

get : url/getMembers/:`${groupID}`
