import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";

import User from "App/Models/User";

export default class UsersController {

    
  public async login({ request, auth }: HttpContextContract) {
    const email = request.input("email");
    const password = request.input("password");

    const token = await auth
        .use("api")
        .attempt(email.toLowerCase(), password, {
          expiresIn: "10 days",
        });


    return token.toJSON();
  }

  public async store({ request, auth, response }: HttpContextContract) {
    const data = request.only(["name", "email", "password", "location"]);
    const user = new User();

    if (
      data.email === undefined ||
      data.password === undefined ||
      data.email.trim() === "" ||
      data.password.trim() === ""
    ) {
      return response.status(400).json({
        message: "email and password required",
        timestamp: new Date(),
      });
    } else {
      await user.merge(data).save();

      const token = await auth.use("api").login(user, {
        expiresIn: "10 days",
      });
      return token.toJSON();
    }
  }

  public async index({ response }: HttpContextContract) {
    const users = await User.all();

    return response.json({ users });
  }

  public async find({ response, params }: HttpContextContract) {
    try {
      const user = await User.find(params.id);

      if (user) {
        return response.json({ user });
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async delete({ params }: HttpContextContract) {
    try {
      const user = await User.find(params.id);

      if (user) {
        await user.delete();
        return "user Deleted Successfully";
      }
    } catch (error) {
      console.log(error);
    }
    return "user not found on servers";
  }

  public async logout({ response, auth }) {
    await auth.use("api").revoke();
    response.status(200).json({
      revoked: true,
    });
  }
}
