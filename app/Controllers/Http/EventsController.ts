import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Event from "App/Models/Event";
export default class EventsController {
  public async store({ request }: HttpContextContract) {
    const eventData = request.only([
      "user_id",
      "title",
      "organizer",
      "location",
      "desc",
    ]);

    const event = new Event();

    await event.merge(eventData).save();

    return event;
  }

  public async index({ response }: HttpContextContract) {
    const events = await Event.all();

    return response.json({ events });
  }

  public async find({ response, params }) {
    try {
      const event = await Event.find(params.id);

      if (event) {
        return response.json({ event });
      }
    } catch (error) {
      console.log(error);
    }
  }

  public async delete({ params }) {
    try {
      const event = await Event.find(params.id);

      if (event) {
        await event.delete();

        return "event Deleted Successfully";
      }
    } catch (error) {
      console.log(error);
    }

    return "event does not exist on servers ";
  }
}
