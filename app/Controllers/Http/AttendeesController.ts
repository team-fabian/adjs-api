import type { HttpContextContract } from "@ioc:Adonis/Core/HttpContext";
import Attendee from "App/Models/Attendee";
export default class AttendeesController {
  public async store({ request }: HttpContextContract) {
    const attendee = new Attendee();

    attendee.eventId = request.input("event_id");

    attendee.userId = request.input("user_id");

    await attendee.save();

    return attendee;
  }

  public async index({ response }: HttpContextContract) {
    const eventsData = await Attendee.all();

    return response.json({ eventsData });
  }

  public async getAttendees({ params, response }) {
    try {
      const eventattendee = await Attendee.query().where(
        "event_id",
        params.groupID
      );

      if (eventattendee) {
        return response.json({ eventattendee });
      }
    } catch (error) {
      console.log(error);
    }

    return "No data found";
  }
}
