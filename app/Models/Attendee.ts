import { DateTime } from "luxon";
import { BaseModel, column, belongsTo, BelongsTo } from "@ioc:Adonis/Lucid/Orm";
import Event from "./Event";
export default class Attendee extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "event_id" })
  public eventId: number;

  @column({ columnName: "user_id" })
  public userId: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => Event)
  public events: BelongsTo<typeof Event>;
}
