import { DateTime } from "luxon";
import {
  BaseModel,
  belongsTo,
  BelongsTo,
  hasMany,
  HasMany,
  column,
} from "@ioc:Adonis/Lucid/Orm";
import User from "./User";
import Attendee from "./Attendee";
export default class Event extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "user_id" })
  public userId: number;

  @column()
  public title: string;

  @column()
  public organizer: string;

  @column()
  public location: string;

  @column()
  public desc: string;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => User)
  public owner: BelongsTo<typeof User>;

  @hasMany(() => Attendee)
  public attendee: HasMany<typeof Attendee>;
}
