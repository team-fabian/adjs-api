import { DateTime } from "luxon";
import { BaseModel, BelongsTo, belongsTo, column } from "@ioc:Adonis/Lucid/Orm";
import Group from "./Group";

export default class Member extends BaseModel {
  @column({ isPrimary: true })
  public id: number;

  @column({ columnName: "group_id" })
  public groupId: number;

  @column({ columnName: "user_id" })
  public userId: number;

  @column.dateTime({ autoCreate: true })
  public createdAt: DateTime;

  @column.dateTime({ autoCreate: true, autoUpdate: true })
  public updatedAt: DateTime;

  @belongsTo(() => Group)
  public groups: BelongsTo<typeof Group>;
}
