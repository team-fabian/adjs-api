/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| This file is dedicated for defining HTTP routes. A single file is enough
| for majority of projects, however you can define routes in different
| files and just make sure to import them inside this file. For example
|
| Define routes in following two files
| ├── start/routes/cart.ts
| ├── start/routes/customer.ts
|
| and then import them inside `start/routes.ts` as follows
|
| import './routes/cart'
| import './routes/customer'
|
*/

import Route from "@ioc:Adonis/Core/Route";

Route.get("/", async () => {
  return "welcome to MEETUP.COM API";
});

// ****************User Routes**************

// create User Request
Route.post("user/store", "UsersController.store").as("user.store");

// Get all users  Request

Route.group(() => {
  // login
  Route.post("user/login", "UsersController.login").as("user.login");

  // logout

  Route.post("user/logout", "UsersController.logout").as("user.logout");

  Route.get("user/all", "UsersController.index").as("user.index");
  // Get Specific user
  Route.get("user/:id", "UsersController.find").as("user.find");

  // delete specific user and all its groups and member data
  Route.delete("user/:id", "UsersController.delete").as("user.delete");

  Route.get("group/all", "GroupsController.index").as("group.index");

  // *******************Group Routes******************
  Route.post("group/store", "GroupsController.store").as("group.store");

  // Get all groups  Request

  // Get Specific group
  Route.get("group/:id", "GroupsController.find").as("group.find");

  // delete specific group and all its groups and member data
  Route.delete("group/:id", "GroupsController.delete").as("group.delete");

  // ***********************Member Routes*************************

  Route.post("member/store", "MembersController.store").as("member.store");

  // Get all members  Request
  Route.get("member/all", "MembersController.index").as("member.index");

  // get all members of a specific group

  Route.get("member/getMembers/:groupID", "MembersController.getAttendees").as(
    "member.getMembers"
  );

  //***************************Events Routes***************************
  Route.post("event/store", "EventsController.store").as("event.store");

  // Get all groups  Request
  Route.get("event/all", "EventsController.index").as("event.index");

  // Get Specific group
  Route.get("event/:id", "EventsController.find").as("event.find");

  // delete specific group and all its groups and member data
  Route.delete("event/:id", "EventsController.delete").as("event.delete");

  //****************************attendees Routes **********************

  Route.post("attendee/store", "AttendeesController.store").as(
    "attendee.store"
  );

  // Get all members  Request
  Route.get("attendee/all", "AttendeesController.index").as("attendee.index");

  // get all members of a specific group

  Route.get(
    "attendee/getMembers/:groupID",
    "AttendeesController.getMembers"
  ).as("attendee.getAttendees");
}).middleware(["auth"]);
